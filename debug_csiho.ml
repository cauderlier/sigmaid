
let main () =
  Arg.parse [] (fun file ->
    Base.labels := [];
    let input = open_in file in
    let lb = Lexing.from_channel input in
    try
      Parse_csiho.file Lex_csiho.token lb
    with
    | e ->
      let start = lb.Lexing.lex_start_p in
      let file = start.Lexing.pos_fname in
      let line = start.Lexing.pos_lnum  in
      let cnum = start.Lexing.pos_cnum - start.Lexing.pos_bol in
      let tok = Lexing.lexeme lb in
      Format.eprintf "Error File: %s, line: %d, column: %d, Token \"%s\"@\n@."
        file line cnum tok;
      raise e)
    "Please provide a file name."

let _ = main ()
