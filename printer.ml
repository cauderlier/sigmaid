(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree
open Scoper
open Typer


let print_id out_fmter (Id i) =
  let s =
    if i == ""
    then "_"
    else (
      if i.[0] >= '0' && i.[0] <= '9' (* identifier starts with a digit, add an underscore in front *)
      then "_" ^ i
      else i)
      in
  Format.fprintf out_fmter "%s" s

let print_cid out_fmter (Cid c) = Format.fprintf out_fmter "%s" c
let print_label out_fmter (Label l) = Format.fprintf out_fmter "lab_%s" l

let compile_string out_fmter = Format.fprintf out_fmter "\"%s\""

let declare_label out_fmter (Label l) =
  Format.fprintf out_fmter "@[<h>def lab_%s@ : string.@]@."
    l
    (* compile_string l *)

let declare_labels out_fmter = List.iter (declare_label out_fmter)

let rec print_ty out_fmter = function
  | Stcid (c, _) -> print_cid out_fmter c
  | Stlist [] ->
    Format.fprintf out_fmter "dk_type.tnil"
  | Stlist ((l, ty) :: ll) ->
    Format.fprintf out_fmter "@[<hov>dk_type.tcons@ %a@ %a@ %a@]"
      print_label l
      print_par_ty ty
      print_par_ty (Stlist ll)
  | Starr (ty1, ty2) ->
    Format.fprintf out_fmter "@[<hov>dk_obj_examples.arrow@ %a@ %a@]"
      print_par_ty ty1
      print_par_ty ty2
  | Stbool ty ->
     Format.fprintf out_fmter "@[<hov>dk_obj_examples.boolT@ %a@]"
       print_par_ty ty
and print_par_ty out_fmter = function
  | Stcid _ | Stlist [] as ty -> print_ty out_fmter ty
  | ty ->
    Format.fprintf out_fmter "(%a)"
      print_ty ty

let rec print_term out_fmter : tterm -> unit = function
  | Tvar (id, _)
  | Tconst (id, _, _) -> print_id out_fmter id
  | Tpar t ->
    Format.fprintf out_fmter "(%a)"
      print_term t
  | Tapp (t1, t2, ty2, ty) ->
    Format.fprintf out_fmter "@[<hov>dk_obj_examples.App@ %a@ %a@ %a@ %a@]"
      print_par_ty ty2
      print_par_ty ty
      print_par_term t1
      print_par_term t2
  | Tabst (x, ty, body, rty) ->
    Format.fprintf out_fmter "@[<hov>dk_obj_examples.Lambda@ %a@ %a@ (%a :@ dk_obj.Expr %a =>@ %a)@]"
      print_par_ty ty
      print_par_ty rty
      print_id x
      print_par_ty ty
      print_term body
  | Ttrue ty ->
     Format.fprintf out_fmter "@[<hov>dk_obj_examples.trueT@ %a@]"
       print_par_ty ty
  | Tfalse ty ->
     Format.fprintf out_fmter "@[<hov>dk_obj_examples.falseT@ %a@]"
       print_par_ty ty
  | Tif (b, t, e, ty) ->
     Format.fprintf out_fmter "@[<hov>dk_obj_examples.ifT@ %a@ %a@ %a@ %a@]"
       print_par_ty ty
       print_par_term b
       print_par_term t
       print_par_term e
  | Tobj (ll, ty) ->
    print_object ty out_fmter ll
  | Tsel (t, l, ty) ->
    Format.fprintf out_fmter "@[<hov>dk_obj_dec.select@ %a@ %a@ %a@]"
      print_par_ty (infer t)
      print_par_term t
      print_label l
  | Tupd (t, l, m, ty) ->
    Format.fprintf out_fmter "@[<hov>dk_obj_dec.update@ %a@ %a@ %a@ %a@]"
      print_par_ty ty
      print_par_term t
      print_label l
      print_par_meth m
  | Tcast (t, ty1, ty2) ->
    Format.fprintf out_fmter "@[<hov>dk_obj.coerce@ %a@ %a@ (@[dk_obj_dec.find_sub@ %a@ %a@])@ %a@]"
      print_par_ty ty1
      print_par_ty ty2
      print_par_ty ty1
      print_par_ty ty2
      print_par_term t
and print_par_term out_fmter = function
  | Tvar _ | Tconst _ | Tpar _ as t -> print_term out_fmter t
  | t ->
    Format.fprintf out_fmter "(%a)"
      print_term t
and print_object ty out_fmter = function
  | [] ->
    Format.fprintf out_fmter "@[<hov>dk_obj.init@ %a@]"
      print_par_ty ty
  | (l, m) :: ll ->
    Format.fprintf out_fmter "@[<hov>dk_obj_dec.update@ %a@ %a@ %a@ %a@]"
      print_par_ty ty
      (print_par_object ty) ll
      print_label l
      print_par_meth m
and print_par_object ty out_fmter t = Format.fprintf out_fmter "(%a)" (print_object ty) t
and print_par_meth out_fmter (Tmeth (x, ty, body, rty)) =
  Format.fprintf out_fmter "(@[<h>%a :@ dk_obj.Expr %a =>@ %a@])"
    print_id x
    print_par_ty ty
    print_term body

let print_line out_fmter = function
  | Ttypedef (cid, ty) ->
    Format.fprintf out_fmter "def @[<h>%a :@ dk_type.type@ := %a.@]@\n"
      print_cid cid
      print_ty ty
  | Tvardef (id, ty, def) ->
    Format.fprintf out_fmter "def @[<h>%a :@ dk_obj.Expr %a@ := %a.@]@\n"
      print_id id
      print_par_ty ty
      print_term def
  | Tcheck (t, ty) ->
    Format.fprintf out_fmter "@[<h>#CHECK %a,@ dk_obj.Expr@ %a.@]@\n"
      print_term t
      print_par_ty ty
  | Tconv (t1, t2, ty) ->
    Format.fprintf out_fmter "@[<h>#CHECK %a,@ dk_obj.Expr@ %a.@]@\n"
      print_term t1
      print_par_ty ty;
    Format.fprintf out_fmter "@[<h>#CHECK %a,@ dk_obj.Expr@ %a.@]@\n"
      print_term t2
      print_par_ty ty;
    Format.fprintf out_fmter "@[<h>#CONV %a,@ %a.@]@\n"
      print_term t1
      print_term t2
  | Tnorm t ->
    Format.fprintf out_fmter "@[<h>#SNF %a.@]@\n"
      print_term t
  | Tprint s ->
     Format.fprintf out_fmter "@[<h>#PRINT \"%s\".@]@\n" s

let print out_fmter = List.iter (print_line out_fmter)
