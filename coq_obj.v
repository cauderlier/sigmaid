(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

Require Import String.

Section types.
  (* Object types are records of types, we define them as association lists *)

  Inductive type :=
  | Empty_type
  | Cons_type : string -> type -> type -> type.

  Theorem type_dec (A B : type) : {A = B} + {A <> B}.
  Proof.
    decide equality.
    apply string_dec.
  Defined.

End types.

Section domains.

  Inductive Position (l : string) (A : type) : type -> Type :=
  | At_head B : Position l A (Cons_type l A B)
  | In_tail B l' C : Position l A B -> Position l A (Cons_type l' C B).

  Theorem Position_dec (l : string) (A B : type) : (Position l A B) + (Position l A B -> False).
  Proof.
    induction B.
    - right; intro H; inversion H.
    - clear IHB1.
      destruct IHB2 as [L | R].
      + left.
        apply In_tail.
        assumption.
      + case (string_dec l s).
        * case (type_dec A B1).
          {
            intros [] []; left; apply At_head.
          }
          {
            intros; right; intro H; inversion H; congruence.
          }
        *   intros; right; intro H; inversion H; congruence.
  Defined.

End domains.

(* Scope of object types *)
Bind Scope obj_type_scope with type.
Delimit Scope obj_type_scope with ty.
(* Scope of object type fields (pairs of strings and types) *)
Delimit Scope type_field_scope with tf.

Section subtyping.

  (* Definition of the subtyping relation: A <: B if B is a subset of A. *)
  Fixpoint Subtype A B {struct B} : Type :=
    match B with
      | Empty_type => True
      | Cons_type l B1 B2 =>
        (Position l B1 A * Subtype A B2)%type
    end.

  Infix "<:" := Subtype (at level 60).

  Lemma subtype_tail {A} {B} l C :
    A <: B -> Cons_type l C A <: B.
  Proof.
    induction B as [ | lB B1 B2].
    - intuition.
    - intros (H, st).
      split.
      + apply In_tail.
        assumption.
      + intuition.
  Defined.

  Lemma subtype_refl A : A <: A.
  Proof.
    induction A as [ | l A HA B HB ].
    - intuition.
    - simpl.
      split.
      + apply At_head.
      + apply subtype_tail.
        assumption.
  Defined.

  Fixpoint domain_subtype {A} {B} :
    A <: B -> (forall l C, Position l C B -> Position l C A).
  Proof.
    intros st l C H.
    destruct B as [ | lB B1 B2 ].
    - inversion H.
    - destruct st as (p, Hst).
      inversion H.
      + assumption.
      + apply (domain_subtype A B2);
        assumption.
  Defined.

  Theorem subtype_trans {A} B {C} : A <: B -> B <: C -> A <: C.
  Proof.
    induction C as [ | lC C1 C2].
    - trivial.
    - intros stAB (HdBC, HstBC).
      split.
      + apply (domain_subtype stAB lC _ HdBC).
      + intuition.
  Defined.

  Theorem subtype_dec A B : (A <: B) + (A <: B -> False).
  Proof.
    induction B; simpl.
    - intuition.
    - clear IHB1.
      destruct IHB2 as [L | R].
      + case (Position_dec s B1 A); intuition.
      + intuition.
  Defined.

  Definition subtype_as_prop A B :=
    match subtype_dec A B with
      | inl _ => True
      | inr _ => False
    end.

  Theorem subtype_dec_correct A B : subtype_as_prop A B -> A <: B.
    unfold subtype_as_prop.
    case (subtype_dec A B); intuition.
  Defined.

End subtyping.

Infix "<:" := Subtype (at level 60).

Definition insert (lA : string * type) (A2 : type) :=
  let (l, A) := lA in Cons_type l A A2.

      (* Notations for object pretypes:
   [ "l₁" : A₁ ; "l₂" : A₂ ; … ; "lₙ" : Aₙ ]
 *)
Notation "l : A" := (l%string, A) (at level 50) : type_field_scope.
Notation "[ x1 ; .. ; xn ]" :=
  (insert x1%tf .. (insert xn%tf Empty_type) ..) : obj_type_scope.
Notation "l : A :: B" := (Cons_type l A B) (at level 100) : type_field_scope.

Section objects.
  (* Objects are records of methods.
   A well-pretyped object of type A = [ lᵢ : Aᵢ ]
   is of the form [ lᵢ = ς(x !: A) tᵢ ] where x : A ⊢ tᵢ : Aᵢ

   The type A appears 3 times in the definition of well-pretyped object of type A :
    - in each ς binder
    - to type each tᵢ
    - to list the labels

   Since we cannot construct an object without breaking the invariant, we define
   preobjects of type (A, C) such that Obj A = Preobject (A, A).

   *)

  (* We have to axiomatize the type of methods because
   Method A B := Expr A -> Expr B has a negative occurence of Expr which blocks
   the recursive definition
   *)

  Parameter Method : type -> type -> Type.

  Inductive Preobject {A : type} : forall (C : type), Type :=
  | poempty : Preobject Empty_type
  | pocons : forall {C} (l : string) (B : type),
               Method A B ->
               Preobject C ->
               Preobject (Cons_type l B C).

  Definition Preobj A C := @Preobject A C.
  Definition Obj (A : type) := Preobj A A.

  (* An expression of type B is an object of type A <: B coerced to type B. *)
  Inductive Expr (B : type) :=
  | Eobj A : Obj A -> (A <: B) -> Expr B.

  (* type, object and prooj of subtyping underlying an ecpression *)
  Definition expr_type {A : type} (e : Expr A) : type.
  Proof.
    destruct e as (A', o, H).
    - exact A'.
  Defined.

  Definition expr_obj {A : type} (e : Expr A) : Obj (expr_type e).
  Proof.
    destruct e.
    unfold expr_type.
    assumption.
  Defined.

  Definition expr_st {A : type} (e : Expr A) : (expr_type e) <: A.
  Proof.
    destruct e.
    unfold expr_type.
    assumption.
  Defined.

  Definition Obj_to_expr {A} (a : Obj A) : Expr A := Eobj A A a (subtype_refl A).

  Definition Expr_coerce {A} B (a : Expr A) (H : A <: B): Expr B
    :=
      match a with
        | Eobj A' o HA => Eobj B A' o (subtype_trans _ HA H)
      end.

  (* End of the axiomatisation of methods: Method A B is equivalent to Obj A -> Obj B. *)
  Parameter Eval_meth : forall {A} {B}, Method A B -> Expr A -> Expr B.
  Parameter Make_meth : forall {A} {B}, (Expr A -> Expr B) -> Method A B.

End objects.

Delimit Scope object_scope with obj.
Delimit Scope method_scope with meth.

Bind Scope method_scope with Method.
Bind Scope object_scope with Preobject.

Section semantics.
  (* Selection and update go inside objects so they have to be defined on preobjects first. *)

  Definition empty_expression : Expr Empty_type := @Obj_to_expr Empty_type (poempty).

  Fixpoint preselect l B {A d} (po : Preobj A d)
           (H : Position l B d) {struct H} : Method A B.
  Proof.
    destruct H.
    - inversion po.
      assumption.
    - inversion po.
      apply (preselect l B A _ X0 H).
  Defined.

  Fixpoint preupdate l B {A d} (po : Preobj A d)
             (H : Position l B d) : Method A B -> Preobj A d.
  Proof.
    intro m.
    destruct H.
    - inversion po.
      apply (pocons _ _ m).
      assumption.
    - inversion po.
      apply pocons.
      + assumption.
      + apply (preupdate l B A _ X0); assumption.
  Defined.

  Definition obj_select l {A B} (a : Obj A) (H : Position l B A) : Expr B
    := Eval_meth (preselect l B a H) (Obj_to_expr a).

  Definition obj_update l {A B} (a : Obj A) (H : Position l B A) (m : Method A B) : Obj A :=
    preupdate l B a H m.

  Definition select l {A B} (a : Expr A) (H : Position l B A) : Expr B.
  Proof.
    destruct a as (C, o, st).
    apply (obj_select l o).
    apply (domain_subtype st).
    assumption.
  Defined.

  Definition update l {A B} (a : Expr A) (H : Position l B A) : Method A B -> Expr A.
  Proof.
    intro m.
    refine (Eobj A (expr_type a) _ (expr_st a)).
    apply (obj_update l (expr_obj a) (domain_subtype (expr_st a) l _ H)).
    apply Make_meth.
    intro self.
    apply (fun s => Expr_coerce A s (expr_st a)) in self.
    exact (Eval_meth m self).
  Defined.

End semantics.

(* It is often practical to let some methods undefined.
   An easy way to do so is to define an initial object
   of each type and define some methods by update. *)
Section init.

  Definition loop_method A B l (H: Position l B A) : Method A B :=
    Make_meth (fun a => @select l A B a H).

  Fixpoint preinit A d {struct d} :
    (forall l B, Position l B d -> Position l B A) -> Preobj A d.
  Proof.
    destruct d as [ | l B C ].
    - intros; apply poempty.
    - intro Hsub.
      apply (pocons l).
      + apply (loop_method _ _ l).
        apply Hsub.
        apply At_head.
      + apply preinit.
        intros l' B' H.
        apply Hsub.
        apply In_tail.
        assumption.
  Defined.

  Definition obj_init A : Obj A := preinit A A (fun _ _ H => H).

  Definition init A : Expr A := Obj_to_expr (obj_init A).
End init.


Section mem.
  (* Finding a position by a method name *)

  Fixpoint mem (l : string) (A : type) : option (sigT (fun B => Position l B A)).
  Proof.
    destruct A.
    - exact None.
    - destruct (string_dec l s).
      + apply Some.
        exists A1.
        destruct e.
        apply At_head.
      + destruct (mem l A2) as [ (B, p) | ].
        * apply Some.
          exists B.
          apply In_tail.
          assumption.
        * exact None.
  Defined.

  Definition mem_as_prop (l : string) (A : type) : Prop :=
    match mem l A with
      | Some _ => True
      | None => False
    end.

  Definition mem_extract_type l A : mem_as_prop l A -> type.
  Proof.
    unfold mem_as_prop.
    case (mem l A).
    - intros (B, _) _; exact B.
    - intros [].
  Defined.

  Definition mem_extract_pos l A : forall H : mem_as_prop l A, Position l (mem_extract_type l A H) A.
  Proof.
    unfold mem_as_prop.
    unfold mem_extract_type.
    case (mem l A).
    - intros (B, p).
      intuition.
    - intros [].
  Defined.

End mem.

Definition mem_extract_select {A : type} (a : Expr A) (l : string) :
  forall H : mem_as_prop l A, Expr (mem_extract_type l A H)
  :=
    fun H => select l a (mem_extract_pos _ _ H).

Notation "a # l" := (mem_extract_select a%obj l%string I) (at level 50) : object_scope.

Definition mem_extract_update (A : type) (o : Expr A) (l : string) (H : mem_as_prop l A) (m : Expr A -> Expr (mem_extract_type l A H)) : Expr A :=
  update _ o (mem_extract_pos l A H) (Make_meth m).

Notation "o ## l ⇐ 'ς' ( x !: A ) m" := (mem_extract_update A%ty o%obj l%string I (fun x => m%obj)) (at level 50).

Section examples.
  (* Encodding of booleans *)
  Definition BoolT A : type :=
    [ "else" : A ; "if" : A ; "then" : A ]%ty.

  Definition init_bool A : Expr (BoolT A) := init (BoolT A).

  Definition trueT A : Expr (BoolT A) :=
    update "if" (init_bool A) (In_tail _ _ _ _ _ (At_head _ _ _)) (Make_meth (fun x => @select "then" _ A x (In_tail _ _ _ _ _ (In_tail _ _ _ _ _ (At_head _ _ _))))).

  Definition falseT A : Expr (BoolT A) :=
    ((init_bool A) ## "if" ⇐ ς(x !: BoolT A) (x # "else")).

  Definition Ifthenelse A b (t e : Expr A) : Expr A :=
    (((b##"then" ⇐ ς(x !: BoolT A) t)##"else" ⇐ ς(x !: BoolT A) e)#"if")%obj.

  (* Encodding of the simply-pretyped λ-calculus *)

  Definition Arrow A B :=
    [ "arg" : A ; "val" : B ]%ty.

  Definition init_arr A B := init (Arrow A B).

  Definition Lambda A B (f : Expr A -> Expr B) : Expr (Arrow A B) :=
    (init_arr A B) ## "val" ⇐ ς(x !: Arrow A B) (f (x#"arg")).

  Definition App A B (f : Expr (Arrow A B)) (a : Expr A) : Expr B :=
    ((f##"arg" ⇐ ς(x !: Arrow A B) a)#"val")%obj.
End examples.

Notation "'ifT' b 'then' t 'else' e" := (Ifthenelse _ b t e) (at level 50) : object_scope.
Infix "→" := Arrow (at level 50).
Notation "'λ' ( x !: A ) b" := (Lambda A _ (fun x : Expr A => b)) (at level 50).
Infix "@" := (App _ _) (at level 50).
