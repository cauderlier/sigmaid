(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree
open Scoper
open Typer

let print_id out_fmter (Id i) =
  let s =
    if i == ""
    then "_"
    else (
      if i.[0] >= '0' && i.[0] <= '9' (* identifier starts with a digit, add an underscore in front *)
      then "_" ^ i
      else i)
      in
  Format.fprintf out_fmter "%s" s

let print_cid out_fmter (Cid c) = Format.fprintf out_fmter "%s" c
let print_label out_fmter (Label l) = Format.fprintf out_fmter "\"%s\"" l

let rec print_ty out_fmter = function
  | Stcid (c, _) -> print_cid out_fmter c
  | Stlist [] -> Format.fprintf out_fmter "Empty_type"
  | Stlist ll ->
     Format.fprintf out_fmter "[@[<hov>%a@]]"
       print_ty_elems ll
  | Starr (ty1, ty2) ->
     Format.fprintf out_fmter "@[<hov>%a@ →@ %a@]"
       print_par_ty ty1
       print_par_ty ty2
  | Stbool ty ->
     Format.fprintf out_fmter "@[<hov>BoolT@ %a@]"
       print_par_ty ty
and print_ty_elem out_fmter (l, ty) =
  Format.fprintf out_fmter "@[<hov>%a :@ %a@]"
    print_label l
    print_par_ty ty
and print_ty_elems out_fmter = function
  | [] -> ()
  | [ el ] -> print_ty_elem out_fmter el
  | el :: els ->
     Format.fprintf out_fmter "%a;@ %a"
       print_ty_elem el
       print_ty_elems els
and print_par_ty out_fmter = function
  | Stcid _ | Stlist _ as ty -> print_ty out_fmter ty
  | ty ->
     Format.fprintf out_fmter "(%a)"
       print_ty ty

let rec print_term out_fmter : tterm -> unit = function
  | Tvar (id, _)
  | Tconst (id, _, _) -> print_id out_fmter id
  | Tpar t ->
     Format.fprintf out_fmter "(%a)"
       print_term t
  | Tapp (t1, t2, _, _) ->
     Format.fprintf out_fmter "@[<hov>%a@ @@@ %a@]"
       print_par_term t1
       print_par_term t2
  | Tabst (x, ty, body, _) ->
     Format.fprintf out_fmter "@[<hov>λ(%a !:@ %a) %a@]"
       print_id x
       print_ty ty
       print_par_term body
  | Ttrue ty ->
     Format.fprintf out_fmter "@[<hov>trueT@ %a@]"
       print_par_ty ty
  | Tfalse ty ->
     Format.fprintf out_fmter "@[<hov>falseT@ %a@]"
       print_par_ty ty
  | Tif (b, t, e, _) ->
     Format.fprintf out_fmter "@[<hov>ifT@ %a@ then@ %a@ else@ %a@]"
       print_term b
       print_term t
       print_par_term e
  | Tobj (ll, ty) ->
     Format.fprintf out_fmter "@[<hov>%a@]"
       (print_obj_elems ty) ll
  | Tsel (t, l, _) ->
     Format.fprintf out_fmter "@[<hov>%a#%a@]"
       print_par_term t
       print_label l
  | Tupd (t, l, m, _) ->
     Format.fprintf out_fmter "@[<hov>%a##%a@ ⇐ %a@]"
       print_par_term t
       print_label l
       print_meth m
  | Tcast (t, ty1, ty2) ->
     Format.fprintf out_fmter "@[<hov>Expr_coerce@ %a@ %a@ (subtype_dec_correct@ %a@ %a@ I)@]"
       print_par_ty ty2
       print_par_term t
       print_par_ty ty1
       print_par_ty ty2
and print_par_term out_fmter = function
  | Tvar _ | Tconst _ | Tpar _ | Tobj _ as t -> print_term out_fmter t
  | t ->
     Format.fprintf out_fmter "(%a)"
       print_term t
and print_obj_elem out_fmter (l, m) =
  Format.fprintf out_fmter "@[<hov>%a ⇐@ %a@]"
    print_label l
    print_meth m
and print_obj_elems ty out_fmter = function
  | [] ->
     Format.fprintf out_fmter "(init@ %a)"
       print_par_ty ty
  | el :: els ->
     Format.fprintf out_fmter "(%a ## %a)"
       (print_obj_elems ty) els
       print_obj_elem el
and print_meth out_fmter (Tmeth (x, ty, body, _)) =
  Format.fprintf out_fmter "@[<h>ς(%a !: %a)%a@]"
    print_id x
    print_ty ty
    print_par_term body

let rec print_line out_fmter = function
  | Ttypedef (cid, ty) ->
     Format.fprintf out_fmter "@[<h>Definition %a :=@ %a%%ty.@]@\n"
       print_cid cid
       print_par_ty ty
  | Tvardef (id, ty, def) ->
     Format.fprintf out_fmter "@[<h>Definition %a :@ Expr %a :=@ %a%%obj.@]@\n"
       print_id id
       print_par_ty ty
       print_par_term def
  | Tcheck (t, ty) ->
     Format.fprintf out_fmter "@[<h>Goal Expr %a@].
Proof.
exact %a%%obj.
Qed.
"
     print_par_ty ty
     print_par_term t
  | Tconv (t1, t2, ty) ->
     print_line out_fmter (Tcheck (t1, ty));
    print_line out_fmter (Tcheck (t2, ty))
  | Tnorm t ->
     Format.fprintf out_fmter "@[<h>Eval Compute %a%%obj.@]@\n"
       print_par_term t
  | Tprint s ->
     Format.fprintf out_fmter "(* \"%s\" *)@\n" s

let print out_fmter = List.iter (print_line out_fmter)
