%{

(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

  open Parsetree

  let parse_error s = print_endline s

  exception Parse_list_error of label * stype * stype
  exception Invalid_char_length of int * int

  let rec print_ty = function
    | Tcid (Cid s) -> Format.eprintf "%s" s
    | Tlist ll ->
      Format.eprintf "[@[";
      print_object ll;
      Format.eprintf "@]]"
    | Tarr (ty1, ty2) ->
      Format.eprintf "(";
      print_ty ty1;
      Format.eprintf ")@ (";
      print_ty ty2;
      Format.eprintf ")";
  and print_object = function
    | [] -> ()
    | [ (Label l, ty) ] ->
      Format.eprintf "%s :@ " l;
      print_ty ty
    | (Label l, ty) :: ll ->
      Format.eprintf "%s :@ " l;
      print_ty ty;
      Format.eprintf " ;@ ";
      print_object ll

  let domain = function
    | Tcid _
    | Tarr _ -> []
    | Tlist ll -> List.map fst ll

%}

%token <string> ID CID LABEL
%token LPAR RPAR DOT COL
%token EPS EXPR PO TNIL TCONS SNIL SCONS MINT_0 MINT_S0 MINT_S1 NAT_0 NAT_S DNIL DCONS ASSOC
%token EXPECTED INFERED ERR_TYPING INCONTEXT
%token <string> ERRORFILE LINE COLUMN QTERM
%token PSEL PUPD
%start prog
%type <unit> prog

%start term
%type <unit> term

%%

prog:  error_msg INCONTEXT context EXPECTED sty DOT INFERED sty DOT
  {
    $1 ();
    $3 ();
    Format.eprintf "Expected: ";
    print_ty $5;
    Format.eprintf ".\nInfered: ";
    print_ty $8;
    Format.eprintf ".\n";
  }
 | error_msg EXPECTED sty INFERED sty
  {
    $1 ();
    Format.eprintf "Expected: ";
    print_ty $3;
    Format.eprintf ".\nInfered: ";
    print_ty $5;
    Format.eprintf ".\n";
  };

error_msg: ERRORFILE LINE COLUMN ERR_TYPING QTERM
  {
    fun () ->
      Format.eprintf "Error file: %s line:%s column:%s Error while typing '%s' in context:\n"
        $1 $2 $3 $5
  }
;

context:   /* empty */ { fun () -> () }
         | decl context { fun () -> ($1 (); $2 ()) }
;

decl: ID COL sty { fun () -> Printf.eprintf "%s: " $1; print_ty $3; Printf.eprintf "\n"};

ty:   CID                       { Tcid (Cid ($1)) }
    | LPAR ty RPAR                { $2 }
    | TNIL                     { Tlist [] }
    | TCONS string ty ty       { match $4 with
                                    | Tlist ll ->
                                      Tlist ((Label $2, $3) :: ll)
                                    | _ -> raise (Parse_list_error (Label $2, $3, $4))
                                  }
;

sty:   EPS LPAR PO ty LPAR ASSOC ty RPAR domain RPAR { assert ($4 = $7); assert ($9 = domain $4); $4 }
     | EXPR ty                                       { $2 }
;

string:   SNIL              { "" }
        | SCONS char string { Printf.sprintf "%c%s" $2 $3 }
        | LPAR string RPAR  { $2 }
        | LABEL             { $1 }
;

char: mint  { let (length, n) = $1 in
              if length = 7 then
                Char.chr n
              else raise (Invalid_char_length (length, n)) };

mint:   MINT_0 { (0, 0) }
      | MINT_S0 nat mint { let (length, n) = $3 in
                              if $2 = length then (length+1, 2*n)
                              else raise (Invalid_char_length (length, n)) }
      | MINT_S1 nat mint { let (length, n) = $3 in
                              if $2 = length then (length+1, 2*n+1)
                              else raise (Invalid_char_length (length, n)) }
      | LPAR mint RPAR     { $2 }
;

nat:   NAT_0 { 0 }
     | NAT_S nat { $2 + 1 }
     | LPAR nat RPAR { $2 }
;

domain:   DNIL { [] }
        | DCONS string domain { (Label $2) :: $3 }
        | LPAR domain RPAR { $2 }
;

term:   PSEL ty LPAR ASSOC ty RPAR domain string term {}
        | PUPD ty LPAR ASSOC ty RPAR domain string term {}
        | ID    {}
;
%%
