{

(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)


  open Parser
  exception Unexpected_char of string
  exception End_of_file_looking_for_double_quotes

  let chars_read = ref ""
  let add_char c = chars_read := Printf.sprintf "%s%c" !chars_read c

  let flush () = chars_read := ""
}

let id = [ 'a'-'z' '_' '0'-'9' ] ['a'-'z' 'A'-'Z' '_' '0'-'9']*

rule token = parse
    | [ ' ' '\t' ] { token lexbuf }
    | '\n' { Lexing.new_line lexbuf; token lexbuf }
    (* The several meanings of "." are disambiguated at lexing *)
    | '.' (id as s) ' '* "<=" { UPDATE (s) }
    | '.' (id as s) ' '* ":=" { FUPD(s) }
    | '.' (id as s) { SELECT (s) }
    | '[' { LBRACK }
    | ']' { RBRACK }
    | ':' { COLUMN }
    | ';' { SEMICOLUMN }
    | '(' { LPAR }
    | ')' { RPAR }
    | "ς" { SIGMA }
    | '=' { EQUAL }
    | "::=" { DEF }
    | '.' { DOT }
    | "->" { ARR }
    | "λ" { LAMBDA }
    | "Bool" { BOOL }
    | "true" { TRUE }
    | "false" { FALSE }
    | "if" { IF }
    | "then" { THEN }
    | "else" { ELSE }
    | "type" { TYPE (Parsing.rhs_start_pos 1) }
    | "let" { VAR (Parsing.rhs_start_pos 1) }
    | "check" { CHECK (Parsing.rhs_start_pos 1) }
    | "norm" { NORM (Parsing.rhs_start_pos 1) }
    | "print" { PRINT (Parsing.rhs_start_pos 1) }
    | id as s { ID (s) }
    | [ 'A'-'Z' ] ['a'-'z' 'A'-'Z' '_' '0'-'9']* as s { CID (s) }
    | '"' { flush (); string lexbuf }
    | _ as c { raise (Unexpected_char (Printf.sprintf"'%c'" c)) }
    | eof { raise End_of_file }

and string = parse
    | "\\n" { add_char '\n'; string lexbuf }
    | '\\' (_ as c) { add_char '\\'; add_char c; string lexbuf }
    | '\n' { Lexing.new_line lexbuf ; add_char '\n'; string lexbuf }
    | '"'  { STRING (!chars_read) }
    | _ as c { add_char c; string lexbuf }
    | eof { raise End_of_file_looking_for_double_quotes }
