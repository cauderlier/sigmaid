# Copyright Raphaël Cauderlier (2014)

# <raphael.cauderlier@inria.fr>

# This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can use,
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided
# only with a limited warranty and the software's author, the holder of
# the economic rights, and the successive licensors have only limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate, and that also
# therefore means that it is reserved for developers and experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards
# their requirements in conditions enabling the security of their
# systems and/or data to be ensured and, more generally, to use and
# operate it in the same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

SKS = $(wildcard *.sk)
DKS = $(SKS:.sk=.dk)
DKOS = $(SKS:.sk=.dko)

# Change COMPILE_MODE to byte if you want bytecode
# instead of machine code
COMPILE_MODE = native

# The destination of the install target, without trailing slash
INSTALL_DIR = /usr/local/bin

.PHONY:	clean depend
.SUFFIXES: .dk .sk .dko .ml .native .byte .v .vo

DKCHECK=dkcheck
SKINDENT=skindent
SKCHECK=skcheck
SKDEP=skdep
DKDEP=dkdep
DKMETA=../dedukti/dkmeta.native
CONFLUENCECHECKER=$(shell locate csiho.sh)
CCOPT= # -cc $(CONFLUENCECHECKER)
DKCHECKOPTIONS=-v -d $(CCOPT)
DKDEPOPTIONS=-v

all: $(DKOS) sigmaid

%.dkm: %.sk
	$(SKINDENT) $< > $@

%.dk: %.dkm unsafe/dk_obj_dec.dko
	$(DKMETA) -I unsafe $< > $@

%.dko: %.dk
	$(DKCHECK) -e $(DKCHECKOPTIONS) $<

builtins.dko: builtins.dk
	$(DKCHECK) -e $(DKCHECKOPTIONS) $<

dk_type.dko: dk_type.sk builtins.dko
	$(SKCHECK) -e $(DKCHECKOPTIONS) $<

dk_obj.dko: dk_obj.sk dk_type.dko builtins.dko
	$(SKCHECK) -e $(DKCHECKOPTIONS) $<

unsafe/dk_obj_dec.dko: unsafe/dk_obj_dec.sk builtins.dko dk_type.dko dk_obj.dko
	cd unsafe && $(SKCHECK) -I .. -e -nl ../$<

test.sk: sigmaid.$(COMPILE_MODE)
	./sigmaid.$(COMPILE_MODE) test.sigma

test.dk: dk_obj_examples.dko

.v.vo:
	coqc $<

.ml.native:
	ocamlbuild -use-menhir $@
.ml.byte:
	ocamlbuild -use-menhir $@

clean:
	rm -rf *.dko *.dkm dk_*.dk *.vo *.glob .depend tmp.* \
            test.sk test.dk test.v \
            unsafe/*.dko unsafe/*.dk \
	    sigmaid sigmaid.native sigmaid.byte _build

sigmaid: sigmaid.$(COMPILE_MODE)
	ln -s sigmaid.$(COMPILE_MODE) sigmaid

install: sigmaid
	install sigmaid $(INSTALL_DIR)/

test.v: sigmaid.$(COMPILE_MODE)
	./sigmaid.$(COMPILE_MODE) test.sigma

test.vo: test.v coq_obj.vo

test: test.dko test.vo

-include .depend
