(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree
open Scoper

type tterm =
| Tvar of id * sty
| Tconst of id * sty * tterm
| Tpar of tterm
| Tapp of tterm * tterm * sty * sty
| Tabst of id * sty * tterm * sty
| Ttrue of sty
| Tfalse of sty
| Tif of tterm * tterm * tterm * sty
| Tobj of (label * tmeth) list * sty
| Tsel of tterm * label * sty
| Tupd of tterm * label * tmeth * sty
| Tcast of tterm * sty * sty
and tmeth = Tmeth of id * sty * tterm * sty

type tline =
| Ttypedef of cid * sty
| Tvardef of id * sty * tterm
| Tcheck of tterm * sty
| Tconv of tterm * tterm * sty
| Tnorm of tterm
| Tprint of string

type typed_tree = tline list

val infer : tterm -> sty
val type_check : Scoper.scoped_tree -> typed_tree

exception Application_of_non_functionnal_value of sty
