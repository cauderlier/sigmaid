(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree
open Scoper

type tterm =
| Tvar of id * sty
| Tconst of id * sty * tterm
| Tpar of tterm
| Tapp of tterm * tterm * sty * sty
| Tabst of id * sty * tterm * sty
| Ttrue of sty
| Tfalse of sty
| Tif of tterm * tterm * tterm * sty
| Tobj of (label * tmeth) list * sty
| Tsel of tterm * label * sty
| Tupd of tterm * label * tmeth * sty
| Tcast of tterm * sty * sty
and tmeth = Tmeth of id * sty * tterm * sty

type tline =
| Ttypedef of cid * sty
| Tvardef of id * sty * tterm
| Tcheck of tterm * sty
| Tconv of tterm * tterm * sty
| Tnorm of tterm
| Tprint of string

type typed_tree = tline list

exception Sty_assoc

let rec sty_assoc l = function
  | Stcid (_, b) -> sty_assoc l b
  | Stlist ll -> List.assoc l ll
  | Starr _
  | Stbool _ -> raise Sty_assoc

exception Application_of_non_functionnal_value of sty
let rec sty_decompose_arrow = function
  | Stcid (_, b) -> sty_decompose_arrow b
  | Stbool _
  | Stlist _ as ty -> raise (Application_of_non_functionnal_value ty)
  | Starr (t1, t2) -> (t1, t2)

let rec subtype (a : sty) : sty -> bool = function
  | Stcid (_, b) -> subtype a b
  | Starr (b1, b2) -> (match a with
    | Starr (a1, a2) -> eq a1 b1 && eq a2 b2
    | _ -> false)
  | Stbool b -> (match a with
    | Stbool a -> eq a b
    | _ -> false)
  | Stlist [] -> true
  | Stlist ((l, bl) :: b) ->
    try
      eq bl (sty_assoc l a) && subtype a (Stlist b)
    with Not_found -> false
and eq a b = subtype a b && subtype b a

let rec infer : tterm -> sty = function
  | Tpar t -> infer t
  | Tvar (_, ty)
  | Tconst (_, ty, _)
  | Tapp (_, _, _, ty)
  | Tif (_, _, _, ty)
  | Tobj (_, ty)
  | Tsel (_, _, ty)
  | Tupd (_, _, _, ty)
  | Tcast (_, _, ty) -> ty
  | Tabst (_, ty, _, rty) -> Starr (ty, rty)
  | Ttrue ty
  | Tfalse ty -> Stbool ty
and infer_meth (Tmeth (_, _, _, ty)) = ty

exception Subtype_checking_error of sty * sty

let tcast t ty1 ty2 =
  if eq ty1 ty2
  then t
  else (
    if subtype ty1 ty2
    then Tcast (t, ty1, ty2)
    else raise (Subtype_checking_error (ty1, ty2)))

exception Different_binder_types of ((label * smeth) list)

let dummy_var = (Id "_self")

(* type_check env ty t
   checks that t is typable with type ty
   and returns a tterm of type ty
   corresponding to t with casts included. *)
let rec type_check env ty : sterm -> tterm = function
  | Svar (x, ty') -> tcast (Tvar (x, ty')) ty' ty
  | Sconst (x, ty', def) -> tcast (Tconst (x, ty', type_check env ty' def)) ty' ty
  | Spar t -> Tpar (type_check env ty t)
  | Sapp (t1, t2) as t ->
    (* We don't know what type should be associated with t2
       so we infer a type for t1 and decompose it as an arrow *)
    let tt1 = type_term env t1 in
    let (ty1l, ty1r) =
      try
        sty_decompose_arrow (infer tt1)
      with Application_of_non_functionnal_value ty' ->
        Format.eprintf "Type error: error while checking %s against %s,
inferred type for %s is %s, an arrow type was expected.@."
          (string_of_sterm t) (string_of_sty ty) (string_of_sterm t1) (string_of_sty ty');
        exit 1
    in
    (* Now we check that the application is typable at type ty *)
    tcast (Tapp (tt1, type_check env ty1l t2, ty1l, ty1r)) ty1r ty
  | Sabst (x, ty', body) as t ->
     (* First decompose ty as an arrow *)
     let (tyl, tyr) =
       try
         sty_decompose_arrow ty
       with Application_of_non_functionnal_value _ ->
         Format.eprintf "Type error: error while checking %s against %s,
the type of an abstraction should be an arrow.@."
          (string_of_sterm t) (string_of_sty ty);
        exit 1 in
     if eq ty' tyl then
       Tabst (x, tyl, type_check ((x, tyl) :: env) tyr body, tyr)
     else
       (* We use eta-expansion to cast the variable x. *)
       Tabst (x, tyl, Tapp (Tabst (x, ty', type_check ((x, ty') :: env) tyr body, tyr),
                            tcast (Tvar (x, tyl)) tyl ty',
                            ty',
                            tyr), tyr)
  | Strue ty' -> (match ty with
    | Stbool ty when eq ty ty' -> Ttrue ty
    | _ -> Format.eprintf "Type error: error while checking true %s against %s,
expected Bool %s.@."
       (string_of_sty ty') (string_of_sty ty) (string_of_sty ty');
      exit 1)
  | Sfalse ty' -> (match ty with
    | Stbool ty when eq ty ty' -> Tfalse ty
    | _ -> Format.eprintf "Type error: error while checking false %s against %s,
expected Bool %s.@."
       (string_of_sty ty') (string_of_sty ty) (string_of_sty ty');
      exit 1)
  | Sif (b, t, e) -> Tif (type_check env (Stbool ty) b,
                         type_check env ty t,
                         type_check env ty e,
                         ty)
  | Sobj [] -> let _ = type_check_object env ty [] in Tobj ([], ty)
  | Sobj ((_, m) :: _ as ll) ->
    let bty = type_meth_binder m in
    let tll = type_check_object env bty ll in
    tcast (Tobj (tll, bty)) bty ty
  | Ssel (t, l) ->
    let tt = type_term env t in
    let ty1 = (
      try sty_assoc l (infer tt)
      with Not_found ->
        (Format.eprintf "Term %s has no method named %s.@."
          (string_of_sterm t) (string_of_label l); exit 1))
    in
    tcast (Tsel (tt, l, ty1)) ty1 ty
  | Supd (t, l, m) ->
    (* Warning, label l could appear in inferred type of t
       but not in ty. This would be correct.
       However, the binder in m tells us the expected type
       which should contain l. *)
    let ty1 = type_meth_binder m in
    (* Now get the type that the method should return.
       and check that the label is OK. *)
    let rty = (
      try sty_assoc l ty1
      with Not_found ->
        (Format.eprintf "Method %s cannot be associated with label %s: label not found in binder type@."
           (string_of_smeth m) (string_of_label l); exit 1))
    in
    tcast (Tupd (type_check env ty1 t, l, type_check_meth env rty m, ty1)) ty1 ty
  | Sfupd (t1, l, t2) ->
    (* Here we have to infer the binder type from t1. *)
    let tt1 = type_term env t1 in
    let tty1 = infer tt1 in
    (* Reduce to regular update case. *)
    type_check env ty (Supd (t1, l, (Smeth (dummy_var, tty1, t2))))

(* Check that an object has type ty as declared in its binders. *)
and type_check_object env ty obj =
  match ty with
  | Stcid (_, b) -> type_check_object env b obj
  | Starr _ ->
    Format.eprintf "Trying to type an object with an arrow type@."; exit 1
  | Stbool _ ->
    Format.eprintf "Trying to type an object with a boolean type@."; exit 1
  | Stlist [] -> (
    match obj with
    | [] -> []
    | (l, _) :: _ -> Format.eprintf "Method %s is not required@." (string_of_label l); exit 1)
  | Stlist ((l, rty) :: lty) -> (
    match obj with
    | [] -> Format.eprintf "Missing method %s@." (string_of_label l); exit 1
    | (l2, _) :: _ when l2 > l -> Format.eprintf "Missing method %s@." (string_of_label l); exit 1
    | (l2, _) :: _ when l2 < l -> Format.eprintf "Method %s is not required@." (string_of_label l2); exit 1
    | (_, m) :: ll ->
      let tm = type_check_meth env rty m in
      let tll = type_check_object env (Stlist lty) ll in
      (l, tm) :: tll)

and type_check_meth env rty (Smeth (x, bty, body)) = Tmeth (x, bty, type_check ((x, bty) :: env) rty body, rty)

and type_term env : sterm -> tterm = function
  | Svar (x, ty) -> Tvar (x, ty)
  | Sconst (x, ty, def) -> Tconst (x, ty, type_check env ty def)
  | Spar t -> Tpar (type_term env t)
  | Sapp (t1, t2) as t ->
    let tt1 = type_term env t1 in
    let (ty1l, ty1r) =
      try
        sty_decompose_arrow (infer tt1)
      with Application_of_non_functionnal_value ty ->
        Format.eprintf "Type error: error while inferring the type of %s,
inferred type for %s is %s, an arrow type was expected.@."
          (string_of_sterm t) (string_of_sterm t1) (string_of_sty ty);
        exit 1
    in
    Tapp (tt1, type_check env ty1l t2, ty1l, ty1r)
  | Sabst (x, ty, body) ->
    let tbody = type_term ((x, ty) :: env) body in
    Tabst (x, ty, tbody, infer tbody)
  | Strue ty -> Ttrue ty
  | Sfalse ty -> Tfalse ty
  | Sif (b, t, e) ->
     let tb = type_term env b in
     let tyb = infer tb in
     (match tyb with
     | Stbool ty -> Tif (tb, type_check env ty t, type_check env ty e, ty)
     | _ -> Format.eprintf "Type error: error while inferring the type of %s,
expecting a boolean.@." (string_of_sterm b); exit 1)
  | Sobj [] -> Tobj ([], Stlist [])
  | Sobj ((_, m) :: _ as ll) ->
    let ty = type_meth_binder m in
    Tobj (type_check_object env ty ll, ty)
  | Ssel (t, l) ->
    let tt = type_term env t in
    let ty1 = (
      try sty_assoc l (infer tt)
      with Not_found ->
        Format.eprintf "Term %s has no method named %s.@."
          (string_of_sterm t) (string_of_label l);
        exit 1)
    in
    Tsel (tt, l, ty1)
  | Supd (t, l, m) ->
    let ty1 = type_meth_binder m in
    let rty = (
      try sty_assoc l ty1
      with Not_found ->
        (Format.eprintf "Method %s cannot be associated with label %s: label not found in binder type@."
          (string_of_smeth m) (string_of_label l); exit 1))
    in
    Tupd (type_check env ty1 t, l, type_check_meth env rty m, ty1)
  | Sfupd (t1, l, t2) ->
    (* Here we have to infer the binder type from t1. *)
    let tt1 = type_term env t1 in
    let tty1 = infer tt1 in
    (* Reduce to regular update case. *)
    type_term env (Supd (t1, l, (Smeth (dummy_var, tty1, t2))))

and type_meth env (Smeth (x, ty, body)) =
  let tbody = type_term ((x, ty) :: env) body in
  (Tmeth (x, ty, tbody, infer tbody))

and type_meth_binder (Smeth (x, ty, body)) = ty

exception Check_failed

let type_line = function
  | Stypedef (a, ty) -> Ttypedef (a, ty)
  | Svardef (id, ty, def) -> Tvardef (id, ty, type_check [] ty def)
  | Scheck (t, ty) ->
    let tt = type_check [] ty t in Tcheck (tt, ty)
  | Sconv (t1, t2, ty) ->
    let tt1 = type_check [] ty t1 in
    let tt2 = type_check [] ty t2 in
    Tconv (tt1, tt2, ty)
  | Snorm t -> Tnorm (type_term [] t)
  | Sprint s -> Tprint s

let type_check = List.map type_line
