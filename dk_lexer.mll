{

(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

  open Dk_parser
  exception Unexpected_char of string
}

let id = [ 'a'-'z' '_' '0'-'9' ] ['a'-'z' 'A'-'Z' '_' '0'-'9' '.' ]*
let num = [ '0'-'9' ]+

rule dktoken = parse
    | "ERROR file: " (id as s) ' ' { ERRORFILE (s) }
    | "line:" (num as n) ' ' { LINE (n) }
    | "column:" (num as n) ' ' { COLUMN (n) }
    | "Expected: " { EXPECTED }
    | "Inferred: " { INFERED }
    | "Error while typing " { ERR_TYPING }
    | "in context:" { INCONTEXT }
    | '\''([^ '\'']+ as s)'\'' { QTERM (s) }
    | [ ' ' '\t' ] { dktoken lexbuf }
    | '\n' { dktoken lexbuf }
    | '(' { LPAR }
    | ')' { RPAR }
    | '.' { DOT }
    | ':' { COL }
    | "pts.eT" { EPS }
    | "dk_obj.preObj" { PO }
    | "dk_obj.Expr" { EXPR }
    | "dk_type.assoc" { ASSOC }
    | "dk_type.nil" { TNIL }
    | "dk_type.cons" { TCONS }
    | "dk_string.nil" { SNIL }
    | "dk_string.cons" { SCONS }
    | "dk_machine_int.O" { MINT_0 }
    | "dk_machine_int.S0" { MINT_S0 }
    | "dk_machine_int.S1" { MINT_S1 }
    | "dk_nat.O" { NAT_0 }
    | "dk_nat.S" { NAT_S }
    | "dk_domain.nil" { DNIL }
    | "dk_domain.cons" { DCONS }
    | "dk_obj.preselect" { PSEL }
    | "dk_obj.preupdate" { PUPD }
    | "lab_" (id as s ) { LABEL (s) }
    | id as s { ID (s) }
    | [ 'A'-'Z' ] ['a'-'z' 'A'-'Z' '_' '0'-'9']* as s { CID (s) }
    | _ as c { raise (Unexpected_char (Printf.sprintf"'%c'" c)) }
    | eof { raise End_of_file }
