%{

(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

  open Parsetree
  let rec insert (k, v) = function
    | [] -> [(k, v)]
    | (k', v') :: l when k < k' -> (k, v) :: (k', v') :: l
    | (k', _) :: l when k = k' -> (k, v) :: l
    | (k', v') :: l -> (k', v') :: (k, v) :: l


  let parse_error s = print_endline s

  let rec insert1 k = function
    | [] -> [k]
    | k' :: l when k < k' -> k :: k' :: l
    | k' :: l when k = k' -> k :: l
    | k' :: l -> k' :: (insert1 k l)

  let label l =
    let l' = Label l in
    Base.labels := insert1 l' !Base.labels; l'

  let rec apply t = function
    | [] -> t
    | t1 :: tl -> apply (App (t, t1)) tl

%}

%token <string> ID CID SELECT UPDATE FUPD STRING
%token LBRACK RBRACK COLUMN SEMICOLUMN LPAR RPAR SIGMA EQUAL DEF DOT
%token LAMBDA ARR BOOL TRUE FALSE IF THEN ELSE
%token <Lexing.position> TYPE VAR CHECK NORM PRINT

%right ARR

%start line
%type <Lexing.position * Parsetree.line> line

%%

line:   TYPE CID DEF ty DOT                     { ($1, Typedef (Cid ($2), $4)) }
      | VAR ID COLUMN ty DEF term DOT           { ($1, Vardef (Id ($2), $4, $6)) }
      | CHECK term COLUMN ty DOT                { ($1, Check ($2, $4)) }
      | CHECK term EQUAL term COLUMN ty DOT     { ($1, Conv ($2, $4, $6)) }
      | NORM term DOT                           { ($1, Norm ($2)) }
      | PRINT STRING DOT                        { ($1, Print ($2)) }
;

ty:   CID                         { Tcid (Cid ($1)) }
    | LBRACK type_elems RBRACK    { Tlist ($2) }
    | ty ARR ty                   { Tarr ($1, $3) }
    | LPAR ty RPAR                { $2 }
    | BOOL LPAR ty RPAR           { Tbool ($3) }
;

type_elem: ID COLUMN ty { (label ($1), $3) };

several_type_elems:   type_elem                               { [$1] }
                    | type_elem SEMICOLUMN several_type_elems { insert ($1) ($3) }
;

type_elems:   /* empty */             { [] }
            | several_type_elems      { $1 }
;

obj:   ID                                 { Var (Id ($1)) }
     | LPAR term RPAR                     { Par ($2) }
     | LBRACK obj_elems RBRACK            { Obj ($2) }
     | TRUE LPAR ty RPAR                  { True ($3) }
     | FALSE LPAR ty RPAR                 { False ($3) }
;

sterm:   obj                                { $1 }
       | obj SELECT                         { Select ($1, label ($2)) }
;

app:   /* empty */ { [] }
     | sterm app   { $1 :: $2 }
;

term:   sterm app                          { apply $1 $2 }
      | LAMBDA LPAR ID COLUMN ty RPAR term { Abst (Id ($3), $5, $7)}
      | obj UPDATE meth                    { Update ($1, label ($2), $3) }
      | obj FUPD sterm                     { Field_update ($1, label ($2), $3) }
      | IF term THEN term ELSE obj         { If ($2, $4, $6) }
;

meth:   SIGMA LPAR ID COLUMN ty RPAR term { Method (Id ($3), $5, $7) }
;

obj_elem: ID EQUAL meth { (label ($1), $3) };

several_obj_elems:   obj_elem { [ $1 ]}
                   | obj_elem SEMICOLUMN several_obj_elems { insert ($1) ($3) }
;

obj_elems:   /* empty */       { [] }
           | several_obj_elems { $1 }
;

%%
