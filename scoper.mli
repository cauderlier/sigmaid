(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree

exception Unknown_type_variable of cid
exception Unknown_variable of id

type sty =
| Stcid of cid * sty
| Stlist of (label * sty) list
| Starr of sty * sty
| Stbool of sty

type sterm =
| Svar of id * sty
| Sconst of id * sty * sterm
| Spar of sterm
| Sapp of sterm * sterm
| Sabst of id * sty * sterm
| Strue of sty
| Sfalse of sty
| Sif of sterm * sterm * sterm
| Sobj of (label * smeth) list
| Ssel of sterm * label
| Supd of sterm * label * smeth
| Sfupd of sterm * label * sterm
and smeth =
| Smeth of id * sty * sterm

type sline =
| Stypedef of cid * sty
| Svardef of id * sty * sterm
| Scheck of sterm * sty
| Sconv of sterm * sterm * sty
| Snorm of sterm
| Sprint of string

type scoped_tree = sline list

val scope : (Lexing.position * Parsetree.line) list -> scoped_tree

val string_of_id : id -> string

val string_of_cid : cid -> string

val string_of_label : label -> string

val string_of_sty : sty -> string

val string_of_sterm : sterm -> string

val string_of_smeth : smeth -> string
