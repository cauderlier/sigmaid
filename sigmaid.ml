(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

let print_pos lb =
    let start = lb.Lexing.lex_start_p in
    let file = start.Lexing.pos_fname in
    let line = start.Lexing.pos_lnum  in
    let cnum = start.Lexing.pos_cnum - start.Lexing.pos_bol in
    let tok = Lexing.lexeme lb in
    Format.eprintf "File: %s, line: %d, column: %d, Token \"%s\"@\n@."
      file line cnum tok

let rec lex_prog lb =
  try
    let (pos, line) = Parser.line Lexer.token lb in
    let lines = lex_prog lb in
    (pos, line) :: lines
  with End_of_file -> []
  | e ->
    print_pos lb;
    raise e

let main () =
  Arg.parse [] (fun file ->
    Base.labels := [];
    let input = open_in file in
    let basefile = Filename.chop_extension file in
    let dk_out =
      Format.formatter_of_out_channel
        (open_out (basefile ^ ".sk"))
    in
    let coq_out =
      Format.formatter_of_out_channel
        (open_out (basefile ^ ".v"))
    in
    let lexbuf = Lexing.from_channel input in
    let prog = lex_prog lexbuf in
    let scoped_prog =
      try Scoper.scope prog
      with Scoper.Unknown_variable id ->
        Format.eprintf "Scope error: unknown variable %s@."
          (Scoper.string_of_id id);
        exit 1
    in
    let typed_prog =
      try Typer.type_check scoped_prog
      with Typer.Application_of_non_functionnal_value ty ->
        Format.eprintf
          "Type error: Application of non functional value.
expected an arrow type, found %s.@."
        (Scoper.string_of_sty ty);
        exit 1
    in
    (* Dedukti output *)
    Format.fprintf dk_out "#NAME %s.@\n" (Filename.chop_extension file);
    Printer.declare_labels dk_out !Base.labels;
    Printer.print dk_out typed_prog;
    Format.fprintf dk_out "@.";
  (* Coq output *)
    Format.fprintf coq_out "Require Import coq_obj.@\n";
    Coq_printer.print coq_out typed_prog;
    Format.fprintf coq_out "@."
  )
    "Please provide a file name."

let _ = main ()
