{
  open Parse_csiho
}

let num = [ '0' - '9' ]+
let id = [ 'a' - 'z' 'A' - 'Z' '_' '0' - '9' '\'' ]+
let unknown_meta = 'm' '_' '?' '_' num

rule token = parse
        | [ ' ' '\t' ] { token lexbuf }
        | '\n' { Lexing.new_line lexbuf; token lexbuf }
        | "NO" { NO }
        | "Problem:" { PROBLEM }
        | "Proof:" { PROOF }
        | "Higher-Order Church Rosser Processor:" { PROCESSOR }
        | "Higher-Order Nonconfluence Processor:" { PROCESSOR }
        | "non-joinable conversion:" { NONJOIN }
        | "Qed" { QED }
        | "critical peaks:" { CRITPEAKS }
        | "<- . ->" { PEAK }
        | "app (lam m_typ (\\v_x. m_F v_x)) m_B -> m_F m_B" {BETA}
        | "->*" { ARRS }
        | "*<-" { LARRS }
        | "->" { ARR }
        | "app" { APP }
        | "lam" { LAM }
        | "pi" { PI }
        | '\\' { BACKSLASH }
        | '.' { DOT }
        | '(' { LPAREN }
        | ')' { RPAREN }
        | num as s { NUM (s)}
        | "c_" (id as s) { CONST (s) }
        | "m_?_" num { UNKNOWNMETA }
        | "m_" (id as s) { META (s) }
        | "v_?" { NOVAR }
        | "v_" (id as s) { VAR (s) }
        | "<-[" (num | ',')* "]->" { PEAK }
        | _ as c { failwith (Printf.sprintf "Unexpected char: '%c'" c) }
        | eof { raise End_of_file }
