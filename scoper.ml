(*

Copyright Raphaël Cauderlier (2014)

<raphael.cauderlier@inria.fr>

This software is a computer program whose purpose is to translate object-oriented program written in the simply-typed ς-calculus from Abadi and Cardelli to Dedukti and Coq.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree

exception Unknown_type_variable of cid
exception Unknown_variable of id

type sty =
| Stcid of cid * sty
| Stlist of (label * sty) list
| Starr of sty * sty
| Stbool of sty

type sterm =
| Svar of id * sty
| Sconst of id * sty * sterm
| Spar of sterm
| Sapp of sterm * sterm
| Sabst of id * sty * sterm
| Strue of sty
| Sfalse of sty
| Sif of sterm * sterm * sterm
| Sobj of (label * smeth) list
| Ssel of sterm * label
| Supd of sterm * label * smeth
| Sfupd of sterm * label * sterm
and smeth =
| Smeth of id * sty * sterm

type sline =
| Stypedef of cid * sty
| Svardef of id * sty * sterm
| Scheck of sterm * sty
| Sconv of sterm * sterm * sty
| Snorm of sterm
| Sprint of string

type scoped_tree = sline list

let rec scope_ty ty_env = function
  | Tcid cid ->
    (try
       Stcid (cid, List.assoc cid ty_env)
     with Not_found ->
       raise (Unknown_type_variable cid))
  | Tlist l ->
    Stlist (List.map
              (fun (lab, ty) -> (lab, scope_ty ty_env ty))
              l)
  | Tarr (ty1, ty2) ->
    Starr (scope_ty ty_env ty1,
           scope_ty ty_env ty2)
  | Tbool ty -> Stbool (scope_ty ty_env ty)

let rec assoc3 v = function
  | [] -> raise Not_found
  | (v', a, b) :: _ when v = v' -> (a, b)
  | _ :: l -> assoc3 v l

let rec scope_term ty_env const_env var_env : term -> sterm = function
  | Var v ->
    (try
       Svar (v, List.assoc v var_env)
     with Not_found -> (
       try
         let (ty, def) = assoc3 v const_env in
         Sconst (v, ty, def)
       with Not_found -> raise (Unknown_variable v)))
  | Par t -> Spar (scope_term ty_env const_env var_env t)
  | App (t1, t2) ->
    Sapp (scope_term ty_env const_env var_env t1,
          scope_term ty_env const_env var_env t2)
  | Abst (x, ty, body) ->
    let sty = scope_ty ty_env ty in
    Sabst (x, sty, scope_term ty_env const_env ((x, sty) :: var_env) body)
  | True ty -> Strue (scope_ty ty_env ty)
  | False ty -> Sfalse (scope_ty ty_env ty)
  | If (b, t, e) -> Sif (scope_term ty_env const_env var_env b,
                        scope_term ty_env const_env var_env t,
                        scope_term ty_env const_env var_env e)
  | Obj l ->
    Sobj (List.map (fun (l, m) -> (l, scope_method ty_env const_env var_env m)) l)
  | Select (t, l) ->
    Ssel (scope_term ty_env const_env var_env t, l)
  | Update (t, l, m) ->
    Supd (scope_term ty_env const_env var_env t,
          l,
          scope_method ty_env const_env var_env m)
  | Field_update (t1, l, t2) ->
    Sfupd (scope_term ty_env const_env var_env t1,
           l,
           scope_term ty_env const_env var_env t2)
and scope_method ty_env const_env var_env (Method (id, ty, body)) =
  let sty = scope_ty ty_env ty in
  Smeth (id, sty, scope_term ty_env const_env ((id, sty) :: var_env) body)

let scope_line ty_env const_env = function
  | Parsetree.Typedef (cid, ty) ->
    let sty = scope_ty ty_env ty in
    ((cid, sty) :: ty_env, const_env, Stypedef (cid, sty))
  | Vardef (id, ty, def) ->
    let sty = scope_ty ty_env ty in
    let sdef = scope_term ty_env const_env [] def in
    (ty_env, (id, sty, sdef) :: const_env, Svardef (id, sty, sdef))
  | Check (t, ty) ->
    let sty = scope_ty ty_env ty in
    let st = scope_term ty_env const_env [] t in
    (ty_env, const_env, Scheck (st, sty))
  | Conv (t1, t2, ty) ->
    let st1 = scope_term ty_env const_env [] t1 in
    let st2 = scope_term ty_env const_env [] t2 in
    let sty = scope_ty ty_env ty in
    (ty_env, const_env, Sconv (st1, st2, sty))
  | Norm t ->
     (ty_env, const_env, Snorm (scope_term ty_env const_env [] t))
  | Print s -> (ty_env, const_env, Sprint s)

let rec scope_prog ty_env const_env = function
  | [] -> []
  | (pos, line) :: prog ->
    let (nty_env, nconst_env, sline) = scope_line ty_env const_env line in
    sline :: scope_prog nty_env nconst_env prog

let scope = scope_prog [] []




let string_of_id (Id s) = s

let string_of_cid (Cid s) = s

let string_of_label (Label s) = s

let rec string_of_sty = function
  | Stcid (cid, sty) -> Printf.sprintf "%s {= %s}" (string_of_cid cid) (string_of_sty sty)
  | Stlist ll ->
    Printf.sprintf "[ %s ]"
      (String.concat " ; "
         (List.map
            (fun (l, sty) ->
              Printf.sprintf "%s : %s"
                (string_of_label l) (string_of_sty sty))
            ll))
  | Starr (sty1, sty2) ->
    Printf.sprintf "(%s -> %s)"
      (string_of_sty sty1)
      (string_of_sty sty2)
  | Stbool sty ->
     Printf.sprintf "(Bool %s)"
       (string_of_sty sty)

let rec string_of_sterm = function
  | Svar (id, sty) -> Printf.sprintf "%s {: %s}"
    (string_of_id id) (string_of_sty sty)
  | Sconst (id, sty, sterm) -> Printf.sprintf "%s {: %s = %s}"
    (string_of_id id) (string_of_sty sty) (string_of_sterm sterm)
  | Spar sterm -> Printf.sprintf "(%s)" (string_of_sterm sterm)
  | Sapp (sterm1, sterm2) -> Printf.sprintf "%s @ %s"
    (string_of_sterm sterm1) (string_of_sterm sterm2)
  | Sabst (id, sty, sterm) -> Printf.sprintf "λ(%s : %s) %s"
     (string_of_id id) (string_of_sty sty) (string_of_sterm sterm)
  | Strue sty -> Printf.sprintf "(true %s)" (string_of_sty sty)
  | Sfalse sty -> Printf.sprintf "(false %s)" (string_of_sty sty)
  | Sif (b, t, e) -> Printf.sprintf "(if %s then %s else %s)"
     (string_of_sterm b) (string_of_sterm t) (string_of_sterm e)
  | Sobj ll ->
    Printf.sprintf "[ %s ]"
      (String.concat " ; "
         (List.map
            (fun (l, m) ->
              Printf.sprintf "%s = %s"
                (string_of_label l) (string_of_smeth m))
            ll))
  | Ssel (sterm, label) -> Printf.sprintf "%s.%s"
    (string_of_sterm sterm) (string_of_label label)
  | Supd (sterm, label, m) -> Printf.sprintf "%s.%s<=%s"
    (string_of_sterm sterm) (string_of_label label) (string_of_smeth m)
  | Sfupd (sterm, label, body) -> Printf.sprintf "%s.%s:=%s"
    (string_of_sterm sterm) (string_of_label label) (string_of_sterm body)
and string_of_smeth (Smeth (id, sty, sterm)) =
  Printf.sprintf "ς(%s : %s) %s"
    (string_of_id id) (string_of_sty sty) (string_of_sterm sterm)
